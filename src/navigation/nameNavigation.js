export const HOME = "HOME"

//drawer
export const USER_INFO = "USER_INFO"
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD'
export const HISTORY_TIMEKEEPING = 'HISTORY_TIMEKEEPING'

//default stack 
export const LOGIN_SCREEN = 'HISTORY_TIMEKEEPING'

//home stack 
export const BEGIN_SCREEN = 'BEGIN_SCREEN'
export const CAMERA_SCREEN = 'CAMERA_SCREEN'
export const USER_DRAWER = 'USER_DRAWER'
export const CHECK_IN_SCREEN = 'CHECK_IN_SCREEN'
