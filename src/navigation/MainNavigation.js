import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

//screen
import BeginScreen from './src/screens/BeginScreen';
import CheckinScreen from './src/screens/ChekinScreen';
import LoginScreen from './src/screens/LoginScreen';

import UserNavigation from './UserNavigation';

const Drawer = createDrawerNavigator();

function MainNavigation() {
  return (
    <NavigationContainer>
      <HomeStack.Navigator screenOptions={{headerShown: false}}>
        <HomeStack.Screen name="beginscreen" component={BeginScreen} />
        <HomeStack.Screen name="camerascreen" component={CameraScreen} />
        <HomeStack.Screen name="loginscreen" component={LoginScreen} />
        <HomeStack.Screen name="user" component={user} />

        {/* <HomeStack.Screen name="historyScreen" component={HistoryScreen} />
        <HomeStack.Screen name="ChangePassword" component={ChangePassword} /> */}
        <HomeStack.Screen name="checkinscreen" component={CheckinScreen} />
      </HomeStack.Navigator>
    </NavigationContainer>
  );
}

export default MainNavigation;
