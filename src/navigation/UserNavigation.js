import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';

//screen
import userInfo from './src/screens/userInfo';
import HistoryScreen from './src/screens/HistoryScreen';
import ChangePassword from './src/screens/ChangePassword';
import BaseDrawer from '../component/BaseDrawer';
import CheckoutToday from '../screens/CheckoutToDay';

const Drawer = createDrawerNavigator();

function UserNavigation() {
  return (
    <Drawer.Navigator drawerContent={props => <BaseDrawer {...props} />}>
      <Drawer.Screen name="historyScreen" component={HistoryScreen} />
      <Drawer.Screen name="ChangePassword" component={ChangePassword} />
      <Drawer.Screen name="userInfo" component={userInfo} />
      <Drawer.Screen name="CheckoutToday" component={CheckoutToday} />
    </Drawer.Navigator>
  );
}
export default UserNavigation;
