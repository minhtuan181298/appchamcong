import * as routesDefault from './nameNavigation';

import BeginScreen from '../screens/BeginScreen';
import CheckInScreen from '../screens/CheckInScreen';
import LoginScreen from '../screens/LoginScreen';
import CameraScreen from '../screens/CameraScreen';
import HistoryScreen from '../screens/HistoryScreen';
import ChangePassword from '../screens/ChangePassword';
import UserInfo from '../screens/UserInfo';
import BaseDrawer from '../component/BaseDrawer';

const routesHome = {
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
}

const routesDrawer = {
    Home: {
        title: 'Trang chủ',
        txt_key: routesDefault?.HOME,
        name: routesDefault?.HOME,
        component: BeginScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    UserInfo: {
        title: 'Thông tin cá nhân',
        txt_key: routesDefault?.USER_INFO,
        name: routesDefault?.USER_INFO,
        component: UserInfo,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    History: {
        title: 'Lịch sử chấm công',
        txt_key: routesDefault?.HISTORY_TIMEKEEPING,
        name: routesDefault?.HISTORY_TIMEKEEPING,
        component: HistoryScreen,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
    ChangePassword: {
        title: 'Đổi mật khẩu',
        txt_key: routesDefault?.CHANGE_PASSWORD,
        name: routesDefault?.CHANGE_PASSWORD,
        component: ChangePassword,
        // iconUnFocus: 'home-outline',
        // iconFocus: 'home',
        options: {},
    },
}
const routesDrawerArray = Object.keys(routesDrawer).map(e => routes[e]);

export {routesDrawer, routesDrawerArray, }