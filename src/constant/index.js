export const BASE_URL = 'http://192.168.1.4:8888';
// export const SERVER_BASE_URL = 'http://192.168.1.4:9999';
export const SERVER_BASE_URL = 'https://timekeeping-management.herokuapp.com';
// export const BASE_URL = 'http://192.168.43.208:8888';
// export const SERVER_BASE_URL = 'http://192.168.43.208:9999';

export const CART_DATA_KEY = '@cartdata_key';
export const USER_DATA_KEY = '@userdata_key';

export const colors = {
  green: '#88cf81',
  yellow: '#ffc854',
  red: '#FF6347',
  violet: '#8F00FF',
  white: '#ffffff',
  blue: '#085F9D',
  blueActive: '#1BC5BD',
};

export const defaultNote = [
  {
    id: 1,
    title: 'Đủ giờ',
    color: '#88cf81',
  },
  {
    id: 2,
    title: 'Nửa công',
    color: '#8F00FF',
  },
  {
    id: 3,
    title: 'Muộn',
    color: '#ffc854',
  },
  {
    id: 4,
    title: 'Nghỉ',
    color: '#FF6347',
  },
];
