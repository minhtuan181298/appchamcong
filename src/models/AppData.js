import { AsyncStorage } from 'react-native'
import users from "../container/test.json"
import moment from 'moment'
import configs from '../configs'

import { _getHistoriesByTime, _insertOneHistory} from "./DatabaseApp"

const KEY_APP_DATA = "appdata"

export class ManagerAppData {
  // mqtt://103.252.1.144:3002 mqtt://192.168.1.231:1883
    static AppDataInstance = null;
    appData = {
        userdata: [],
        historydata:[],
        track_id: 0,
        config:{
            SRC_ID:"src_id_11928",
            URI:"mqtt://103.252.1.144:3002",
            clientId:'clientId_11928',
            time:30 * 86400,
            timeupdate: 100000
        },
    };
    client = null

    key = KEY_APP_DATA

    Observers = [];

    addObserver(observer) {
        this.Observers.push(observer)
    }
    removeObserver(o) {
        this.Observers = this.Observers.filter((observer) => observer != o);
    }

    static getAppDataInstance() {
        if (ManagerAppData.AppDataInstance == null) {
            ManagerAppData.AppDataInstance = new ManagerAppData();
            this.AppDataInstance.loadDataFromLocal();
            setInterval(()=>{
                ManagerAppData.AppDataInstance.autoCheckTime()
            }, 1000);
            // setInterval(()=>{
            //     ManagerAppData.AppDataInstance.autoUpdateHistoryToWindow()
            // }, configs.timeupdate)

        }
        return this.AppDataInstance;
    }
   

    removeHistory(){
        this.appData.historydata = []  
        this.updateDataToLocal(this.appData,this.key)
    }

    updateDataConfig(config) {
        this.appData.config = config
        AsyncStorage.removeItem(KEY_APP_DATA, (error) => {
            if (error == null) {
                AsyncStorage.setItem(KEY_APP_DATA, JSON.stringify(this.appData))
            }
        })
        this.listenChange()
    }
    getUserByID(rid){
        for (let i = 0; i < this.appData.userdata.length; i++) {
            const ele = this.appData.userdata[i];
            if (ele.id == rid) {
                return ele
            }
        }
        return false
    }


    addHistory(item, track_id){

        let tmp = this.getUserByID(item.recognize_id)
       item.avatar = tmp.avt
       console.log("item", item) //detected_face
        item.department = tmp.department
        let time = moment(item.updatedAt)
        item.time = time.unix()
        //item.time = time.format('HH:mm:ss DD/MM/YYYY ')
        this.appData.historydata.splice(0, 0, item)
        this.appData.track_id = track_id
        if (this.appData.historydata.length > 10) {
            this.appData.historydata.pop();
        }
        this.client.publish('topic/RecognitionLogs', JSON.stringify({is_open: "true"}), 2, true)

        _insertOneHistory(item)
        this.updateDataToLocal(this.appData,this.key)
    }

    autoCheckTime(){
        // if(this.appData.historydata.length < 2) return ;
        // var check =  moment(this.appData.historydata[this.appData.historydata.length - 1].time.format('HH:mm:ss DD/MM/YYYY ') )
        // if (moment().diff(check, 'seconds') > configs.time) {
        //     this.appData.historydata.pop()
        // }
        // this.updateDataToLocal(this.appData,this.key)
    }

    autoUpdateHistoryToWindow(start, end){
       console.log('history', this.appData.historydata)
       _getHistoriesByTime(start, end, this.client) 
    //    setTimeout(()=>{
    //        console.log(this.client)
    //     //this.client.publish('topic/RecognitionLogs', JSON.stringify(tmp), 2, true)
    //    }, 3000)
    }

    

    addUser(arrUser){
        let tmp = Object.assign([], this.appData.userdata);
        if (Array.isArray(arrUser)) {
            for (let i = 0; i < arrUser.length; i++) {
                const element = arrUser[i];
                tmp.push(element)
            }
        }
        this.appData.userdata = tmp
        console.log("his.appData.userdata 1", this.appData.userdata)

        this.updateDataToLocal(this.appData,this.key)
    }

    getArrayUserID(){
        let tmp = []
        console.log("his.appData.userdata 2", this.appData.userdata)
        this.appData.userdata.forEach(element => {
            tmp.push({
                id:element.id,
                ver: element.ver
            })
        });
        return tmp
    }

    // Lấy user vừa check in được
    getDetailUser(id){
        this.appData.historydata.forEach(element => {
            if(element.id == id){
                return element
            }
        });
    }
    getDataHistory(){
       
        return this.appData.historydata
    }
    getCurrentTrackID(){
        return `${this.appData.track_id}`
        // console.log("this.appData.historydata[0].tracking_id >>0",`${parseInt(this.appData.historydata[0].tracking_id) >>0 + 1}`)
        // return this.appData.historydata.length == 0? '0':`${parseInt(this.appData.historydata[this.appData.historydata.length-1].tracking_id) + 1}`
    }
    // 
    getAppData() {
        return this.appData;
    }
    listenChange() {
        this.Observers.map(
            (o) => {
                if (o.onAppDataChange) {
                    o.onAppDataChange(this)
                }
            }
        )
    }

    updateDataToLocal(appData, key) {
       // console.log(key);

        this.appData = appData
        this.key = key
        AsyncStorage.removeItem(key, (error) => {
            if (error == null) {
                AsyncStorage.setItem(key, JSON.stringify(this.appData)), (error) => {
                  //  console.log(error)
                }
                this.listenChange()
            }
        })
        //this.listenChange()
    }

    loadDataFromLocal() {
        AsyncStorage.getItem(this.key, (error, result) => {
            if (error == null) {
                if (result != null) {
                    this.appData = JSON.parse(result);
                    this.appData.track_id = 0

                   // console.log(this.key, this.appData)
                   console.log('test', JSON.stringify(this.appData.userdata) )

                    this.listenChange()
                } else {
                    // this.appData = {
                    //     currentPlay: {
                    //         audioPlayId: 0,
                    //         secondPlay: 0,
                    //     }
                    // }
                }
            }
        })
    }
}