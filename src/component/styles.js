import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../constant';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  widthDevice: width,
  heightDevice: height,

  calendarContainer: {
    borderWidth: 0.5,
    borderColor: colors.green,
  },
});

export default styles;
