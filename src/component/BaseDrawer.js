import React from 'react';
import {ThemeProvider, useNavigation} from '@react-navigation/native';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import asset from '../asset';

export default function BaseDrawer(props) {
  const {state} = props;
  const {navigate} = useNavigation();

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          height: 200,
          backgroundColor: '#02488e',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image style={styles.img_logo} source={asset.images.logo} />
      </View>
      <DrawerContentScrollView>
        <DrawerItem
          label={'Quản lý chấm công'}
          onPress={() => {
            navigate('timekeepingManager');
          }}
          style={{
            ...styles.drawerItemStyle,
            ...styles.drawerItemBorderBottom,
          }}
          labelStyle={styles.drawerItemLabelStyle}
        />
        <DrawerItem
          label={'Chấm công hôm nay'}
          onPress={() => {
            navigate('CheckoutToday');
          }}
          style={{
            ...styles.drawerItemStyle,
            ...styles.drawerItemBorderBottom,
          }}
          labelStyle={styles.drawerItemLabelStyle}
        />
        <DrawerItem
          label={'Lịch sử chấm công'}
          //   icon={() => (
          //     <View style={{width: 25}}>
          //       <Image
          //         style={{resizeMode: 'cover', width: 20, height: 20}}
          //         source={asset.images.logo}
          //       />
          //     </View>
          //   )}
          onPress={() => {
            navigate('historyScreen');
          }}
          style={{
            ...styles.drawerItemStyle,
            ...styles.drawerItemBorderBottom,
          }}
          labelStyle={styles.drawerItemLabelStyle}
        />
        <DrawerItem
          label={'Thông tin nhân viên'}
          //   icon={() => (
          //     <View style={{width: 25}}>
          //       <Image
          //         style={{resizeMode: 'cover', width: 20, height: 20}}
          //         source={asset.images.logo}
          //       />
          //     </View>
          //   )}
          onPress={() => {
            navigate('userInfo');
          }}
          style={{
            ...styles.drawerItemStyle,
            ...styles.drawerItemBorderBottom,
          }}
          labelStyle={styles.drawerItemLabelStyle}
        />
        <DrawerItem
          label={'Đổi mật khẩu'}
          //   icon={() => (
          //     <View style={{width: 25}}>
          //       <Image
          //         style={{resizeMode: 'cover', width: 20, height: 20}}
          //         source={asset.images.logo}
          //       />
          //     </View>
          //   )}
          onPress={() => {
            navigate('ChangePassword');
          }}
          style={{
            ...styles.drawerItemStyle,
            ...styles.drawerItemBorderBottom,
          }}
          labelStyle={styles.drawerItemLabelStyle}
        />
        <View style={styles.logOutButtonContainer}>
          <TouchableOpacity
            style={styles.logOutButton}
            onPress={() => {
              navigate('beginscreen');
            }}>
            <Text style={styles.logOutText}>Đăng xuất</Text>
          </TouchableOpacity>
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  img_logo: {
    width: 110,
    height: 110,
  },
  logOutButton: {
    flexDirection: 'row',
    backgroundColor: '#E6E6E6',
    justifyContent: 'center',
    paddingVertical: 5,
    borderRadius: 3,
  },
  logOutButtonContainer: {
    width: '100%',
    padding: 20,
  },
  logOutText: {
    color: '#3D3D3D',
    fontSize: 16,
  },
  buttonDrawer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 32,
    borderBottomWidth: 0.5,
    borderColor: 'gray',
    paddingLeft: '10%',
  },
  textElement: {
    fontWeight: 'bold',
    color: '#020202',
  },
  icons: {
    color: 'gray',
  },
  drawerItemStyle: {
    width: '100%',
    paddingBottom: 5,
    fontWeight: '100',
  },
  drawerItemBorderBottom: {
    borderBottomWidth: 0,
    borderBottomColor: '#CECECE',
  },
  drawerItemLabelStyle: {
    color: '#3D3D3D',
    fontSize: 16,
  },
});
