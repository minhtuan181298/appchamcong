import React from 'react';
import {Calendar} from 'react-native-calendars';
import styles from './styles';
import {colors} from '../constant';

function CalendarComponent(props) {
  const {markedDates, onGetDetailDate} = props;
  return (
    <Calendar
      //   current={props.state.day}
      calendarWidth={styles?.widthDevice}
      calendarHeight={320}
      style={styles?.calendarContainer}
      hideExtraDays
      onDayPress={day => {
        onGetDetailDate(day);
      }}
      //   onVisibleMonthsChange={months => {
      //     props._onVisibleMonthsChange(months);
      //   }}
      markingType={'period'}
      markedDates={markedDates}
      theme={{
        textDayFontWeight: '300',
        textMonthFontWeight: 'bold',
        textDayHeaderFontWeight: '300',
        textDayFontSize: 16,
        textMonthFontSize: 16,
        textDayHeaderFontSize: 16,
        monthTextColor: colors.blue,
      }}
    />
  );
}

export default CalendarComponent;
