import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function NoteComponent(props) {
  const {color, title, id} = props;
  return (
    <View style={styles?.viewRow} key={id}>
      <View style={[styles?.viewNoteColor, {backgroundColor: color}]} />
      <Text style={styles?.txtTitle}>{title}</Text>
    </View>
  );
}

export default NoteComponent;

const styles = StyleSheet.create({
  viewRow: {
    flexDirection: 'row',
    height: 40,
    width: 200,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  viewNoteColor: {
    height: 30,
    width: 30,
    borderRadius: 5,
  },
  txtTitle: {
    fontSize: 16,
    paddingHorizontal: 10,
  },
});
