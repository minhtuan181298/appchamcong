/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import MonthPicker from 'react-native-month-year-picker';
import moment from 'moment';
import asset from '../asset';
import {getTimekeeping} from '../api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  convertDateToDay,
  convertTimestampToStringTime,
} from '../ulis/convertDateTime';

const HistoryScreen = ({navigation}) => {
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [data, setData] = useState([]);
  const [dataTime, setDataTime] = useState([]);
  const [month, setMonth] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const id = await AsyncStorage.getItem('data');
    const data = await getTimekeeping(id);
    setDataTime(data.data);
  };

  const renderListHistoryReferral = () => {
    return data.map((item, index) => {
      const tmp = dataTime.filter(time => time.tk_date === item.timestamp);
      // console.log(tmp);
      return (
        <View style={styles.containerElementListReferral} key={index}>
          <View style={styles.containerNumericalOrder}>
            <View style={styles.viewChildrenNumberical}>
              <View style={styles.circleNumber}>
                <Text style={styles.textNumberOrder}>
                  {item?.name.slice(item?.name.length - 2, item?.name.length)}
                </Text>
              </View>
            </View>

            <View style={{justifyContent: 'center'}}>
              <Text style={styles.textInviteSuccess}>
                {item?.name.slice(0, item?.name.length - 3)}
              </Text>
            </View>
          </View>
          <View style={styles.containerViewEmail}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{width: 55}}>Giờ vào:</Text>
              <Text style={styles.textEmail}>
                {tmp.length > 0
                  ? convertTimestampToStringTime(tmp[0]?.ip_time)
                  : null}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{width: 55}}>Giờ ra:</Text>
              <Text style={styles.textEmail}>
                {tmp.length > 0 && tmp[0].op_time
                  ? convertTimestampToStringTime(tmp[0]?.op_time)
                  : null}
              </Text>
            </View>
          </View>
        </View>
      );
    });
  };
  const getDaysInMonth = (month, year) => {
    let date = new Date(year, month, 1);
    let days = [];
    let convert = [];
    let timestamp = [];
    while (date.getMonth() === month) {
      days.push(new Date(date).toDateString());
      timestamp.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    for (let i = 0; i < days.length; i++) {
      convert.push({
        name:
          convertDateToDay(days[i].slice(0, 3)) + ' ' + days[i].slice(8, 10),
        timestamp: moment(timestamp[i], 'DD/MM/YYYY').unix(),
      });
    }
    setData(convert);
  };

  const onValueChange = useCallback(
    (event, newDate) => {
      const selectedDate = newDate || date;
      setShow(false);
      const month = moment(selectedDate)
        .format('dd/MM/YYYY')
        .slice(3, 5);
      const year = moment(selectedDate)
        .format('dd/MM/YYYY')
        .slice(6, 10);
      getDaysInMonth(Number(month) - 1, Number(year));
      setMonth(month);
      setShow(false);
      setDate(selectedDate);
    },
    [date],
  );
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#02488e',
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 15,
          marginBottom: 30,
        }}>
        <TouchableOpacity
          style={{width: 20, height: 20}}
          onPress={() => {
            navigation.openDrawer();
          }}>
          <Image style={{width: 20, height: 20}} source={asset.icons.menu} />
        </TouchableOpacity>
        <Text style={{fontSize: 20, fontWeight: 'bold', color: '#fff'}}>
          Lịch sử chấm công
        </Text>
        <Image style={{width: 20, height: 20}} />
      </View>
      <TouchableOpacity
        onPress={() => setShow(true)}
        style={{
          width: 150,
          height: 30,
          borderWidth: 0.5,
          borderStyle: 'solid',
          borderRadius: 5,
          justifyContent: 'center',
          marginLeft: 10,
          marginBottom: 10,
        }}>
        <Text style={{fontSize: 14, marginLeft: 5}}>Tháng {month}</Text>
      </TouchableOpacity>
      {show && (
        <MonthPicker
          onChange={onValueChange}
          value={date}
          maximumDate={new Date(2025, 7)}
          locale="vi"
        />
      )}
      <ScrollView>{renderListHistoryReferral()}</ScrollView>
    </View>
  );
};

export default HistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerElementListReferral: {
    flexDirection: 'row',
    borderColor: '#fff',
    borderBottomColor: '#E2E2E2',
    borderStyle: 'solid',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  containerNumericalOrder: {
    flex: 5,
    flexDirection: 'row',
  },
  viewChildrenNumberical: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 20,
  },
  circleNumber: {
    width: 35,
    height: 35,
    backgroundColor: '#02488e',
    borderRadius: 130,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNumberOrder: {
    color: '#fff',
    fontWeight: '500',
  },
  textInviteSuccess: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#515151',
  },
  textTimeInvite: {
    fontSize: 14,
    color: '#8E8E93',
    marginTop: 10,
  },
  containerViewEmail: {
    flex: 1.8,
    alignItems: 'flex-start',
  },
  textEmail: {
    color: '#8E8E93',
    fontSize: 14,
    fontStyle: 'italic',
  },
});
