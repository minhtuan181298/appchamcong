import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import asset from '../asset';
import Toast from 'react-native-simple-toast';
import {Login} from '../api/index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {signIn} from '../api/authApis';
import {getToken} from '../ulis/asyncStorage';

function LoginScreen(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [spinner, setSpinner] = useState(false);

  const onLogin = async () => {
    if (username && password) {
      setSpinner(true);
      const data = await signIn({username, password});
      if (data) {
        props.navigation.navigate('user');
      } else {
        Toast.show(
          'Đăng nhập không thành công kiểm tra lại tài khoản hoặc mật khẩu',
          Toast.SHORT,
          Toast.TOP,
        );
      }
      setSpinner(false);
    } else {
      Toast.show('Vui lòng nhập đủ thông tin', Toast.SHORT, Toast.TOP);
    }
  };

  return (
    <ScrollView>
      <Spinner visible={spinner} textStyle={styles.spinnerTextStyle} />
      <View style={styles.view_body}>
        <View style={styles.view_top}>
          <View style={styles.viewLogin}>
            <TouchableOpacity
              onPress={() => {
                props.navigation.goBack();
              }}>
              <Image
                style={{width: 30, height: 30}}
                source={asset.icons.back_white}
              />
            </TouchableOpacity>
            <Text style={styles.txt_trueid}>Đăng nhập</Text>
            <Text />
          </View>
          <Image style={styles.img_logo} source={asset.images.logo} />
          <View style={styles.view_txt_login} />
        </View>
        <View style={styles.view_container}>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Domain</Text>
            <TextInput
              style={styles.inp_uname}
              onChangeText={value => setUsername(value)}
            />
          </View>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Mật khẩu</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.inp_uname}
              onChangeText={value => setPassword(value)}
            />
          </View>
        </View>
        <View style={styles.view_btn}>
          <TouchableOpacity style={styles.btn_login} onPress={onLogin}>
            <Text style={styles.txt_btn_login}>Đăng nhập</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}
export default LoginScreen;

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  view_body: {
    width: DEVICE_WIDTH,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  view_top: {
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT / 2.1,
    backgroundColor: '#02488e',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  view_container: {
    width: '93%',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    marginTop: 30,
    borderRadius: 5,
    paddingBottom: 15,
    padding: 5,
  },
  view_btn: {
    width: DEVICE_WIDTH,
    flexDirection: 'column',
    alignItems: 'center',
    padding: 40,
    paddingBottom: 20,
  },
  view_email: {
    width: '93%',
    flexDirection: 'column',
  },
  view_txt_login: {
    width: DEVICE_WIDTH,
    alignItems: 'center',
  },
  inp_uname: {
    width: '100%',
    height: 50,
    borderBottomColor: '#02488e',
    borderBottomWidth: 1,
    fontSize: 20,
  },
  txt_login: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  btn_login: {
    width: '93%',
    height: 50,
    backgroundColor: '#02488e',
    borderRadius: 5,
    justifyContent: 'center',
  },
  txt_btn_login: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
  btn_register: {
    width: 120,
    height: 45,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#020202',
    padding: 20,
  },
  txt_register: {
    textAlign: 'center',
    padding: 7,
    fontWeight: 'bold',
    fontSize: 18,
  },
  view_forgot_pass: {
    flexDirection: 'row',
  },
  viewLogin: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    width: DEVICE_WIDTH,
    marginTop: 20,
    paddingLeft: 10,
  },

  txt_trueid: {
    fontSize: 23,
    color: '#ffffff',
    fontWeight: 'bold',
    marginLeft: -20,
  },
  txt_login: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: 'bold',
  },
  txt_voice: {
    fontSize: 20,
    color: '#ffffff',
    paddingBottom: 30,
  },
  img_logo: {
    width: 110,
    height: 110,
  },
  txt_email: {
    fontSize: 18,
    color: '#C7C7CF',
    paddingTop: 5,
  },
  txt_signup: {
    fontSize: 16,
    color: '#02488e',
    fontWeight: 'bold',
  },
  txt_donthave: {
    fontSize: 16,
    color: '#02488e',
  },
  txt_forgot_pass: {
    fontSize: 16,
    color: '#020202',
  },
  btn_signup: {
    flexDirection: 'column',
    marginTop: 0,
    alignItems: 'center',
  },
});
