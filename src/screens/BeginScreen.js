import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';

function beginScreen(props) {
  const [check, setCheck] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setCheck(false);
    }, 3000);
  }, []);
  return check ? (
    <View style={styles.viewContainer}>
      <AnimatedLoader
        visible={check}
        source={require('../asset/json/clock-in.json')}
        animationStyle={styles.lottie}
        speed={0.9}
        loop={false}
      />
    </View>
  ) : (
    <View style={styles.viewContainer}>
      <TouchableOpacity
        style={styles.viewCol1}
        onPress={() => props.navigation.navigate('camerascreen')}>
        <Text style={styles.txt}>Chấm công</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.viewCol2}
        onPress={() => props.navigation.navigate('loginscreen')}>
        <Text style={styles.txt}>Đăng nhập</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#02488e',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewCol1: {
    width: 140,
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewCol2: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#fff',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {
    color: '#02488e',
    fontSize: 17,
    fontWeight: '700',
  },
  lottie: {
    width: 500,
    height: 500,
  },
});
export default beginScreen;
