import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';
import Icon from 'react-native-vector-icons/AntDesign';
import {GetDetailCheckout} from '../api/timekeepingApis';
import asset from '../asset';
import {getUserInfo} from '../ulis/asyncStorage';

function CheckoutToday(props) {
  const [check, setCheck] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchCheckoutToday();
  }, []);

  const fetchCheckoutToday = async () => {
    const dateNow = Date.now();
    const date = moment(dateNow).format('YYYY-MM-DD');
    console.log('dateNow', date);
    const objId = await getUserInfo();
    const dataCheckout = await GetDetailCheckout(objId.userObjId, date);
    setData([...dataCheckout.data]);
  };
  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginHorizontal: 10,
          marginVertical: 5,
        }}>
        <Icon
          name={item.type === 'CheckIn' ? 'login' : 'logout'}
          color={item.type === 'CheckIn' ? 'green' : 'red'}
          size={20}
        />
        <View>
          <Text style={{marginLeft: 5, fontSize: 12, fontWeight: 'bold'}}>
            Thời gian: {item?.date.split(' ')[1]}
          </Text>
          <Text style={{marginLeft: 5}}>
            Địa điểm: {item?.branchNearest?.name}
          </Text>
          <Text style={{marginLeft: 5}}>
            Khoảng cách: {parseInt(item?.distance, 10)}m
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#02488e',
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 15,
          marginBottom: 30,
        }}>
        <TouchableOpacity
          style={{width: 20, height: 20}}
          onPress={() => {
            props.navigation.openDrawer();
          }}>
          <Image style={{width: 20, height: 20}} source={asset.icons.menu} />
        </TouchableOpacity>
        <Text style={{fontSize: 20, fontWeight: 'bold', color: '#fff'}}>
          Chấm công hôm nay
        </Text>
        <Image style={{width: 20, height: 20}} />
      </View>
      <View style={styles.viewContainer}>
        <TouchableOpacity
          style={styles.viewCol1}
          onPress={() => props.navigation.navigate('camerascreen')}>
          <Text style={styles.txt}>Chấm công</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          marginTop: 30,
          backgroundColor: 'rgba(27, 24, 24, 0.12)',
        }}>
        {data.length == 0 ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 150,
            }}>
            <Text>No data</Text>
          </View>
        ) : (
          <FlatList data={data} renderItem={renderItem} />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: 'white',
    alignItems: 'center',
  },
  viewCol1: {
    width: 140,
    height: 50,
    backgroundColor: '#02488e',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewCol2: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#02488e',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {
    color: '#fff',
    fontSize: 17,
    fontWeight: '700',
  },
  lottie: {
    width: 500,
    height: 500,
  },
});
export default CheckoutToday;
