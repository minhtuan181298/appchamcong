/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, createRef} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import CalendarComponent from '../component/CalendarComponent';
import asset from '../asset';
import {fetchListDateByUser, GetDetailCheckout} from '../api/timekeepingApis';
import {getItem, getUserInfo} from '../ulis/asyncStorage';
import {renderColor} from '../ulis';
import Icon from 'react-native-vector-icons/AntDesign';
import NoteComponent from '../component/NoteComponent';
import { defaultNote } from '../constant';
import ActionSheet from 'react-native-actions-sheet';

const initialFilter = {

};
const actionSheetRef = createRef();
function TimekeepingManager(props) {
  let actionSheet;
  const {navigation} = props;

  const [data, setData] = useState([]);
  const [timeChose, setTimeChose] = useState();
  const [dateSelected, setDateSelected] = useState(false);
  const [filterDetail, setFilterDetail] = useState(false);
  const [listMarkedDate, setMarkedDate] = useState({});

  useEffect(() => {
    fetchListTimekeeping();
  }, []);

  const onGetDetailDate = async item => {
    setTimeChose(item);
    const objId = await getUserInfo();
    const dataCheckout = await GetDetailCheckout(objId.userObjId,item.dateString);
    console.log('1234',dataCheckout.data);
    setData([...dataCheckout.data]);
    actionSheetRef.current?.setModalVisible();
    console.log({item});
  };

  const handleDataTimekeeping = array => {
    let listTmp = {};
    if (array && Array.isArray(array) && array.length > 0) {
      array.map(item => {
        const color = renderColor(item?.status);
        listTmp[(item?.date)] = {
          _id: item?._id,
          userObjId: item?.userObjId,
          color,
          selected: true,
          startingDay: true,
          endingDay: true,
        };
      });
    }
    return listTmp;
  };

  async function fetchListTimekeeping() {
    const userInfo = await getItem('USER_INFO');
    if (userInfo) {
      const params = {
        userObjId: userInfo?.userObjId,
      };
      const result = await fetchListDateByUser(params);
      if (!result?.error && result?.data) {
        const resultState = handleDataTimekeeping(result?.data);
        setMarkedDate(resultState);
      }
    }
  }


  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginHorizontal: 10,
          marginVertical: 5,
        }}>
        <Icon
          name={item.type === 'CheckIn' ? 'login' : 'logout'}
          color={item.type === 'CheckIn' ? 'green' : 'red'}
          size={20}
        />
        <View>
        <Text style={{marginLeft: 5, fontSize: 12, fontWeight: 'bold'}}>Thời gian: {item?.date.split(' ')[1]}</Text>
        <Text style={{marginLeft: 5}}>Địa điểm: {item?.branchNearest?.name}</Text>
        <Text style={{marginLeft: 5}}>Khoảng cách: {parseInt(item?.distance, 10)}m</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles?.container}>
      <View style={styles?.viewHeader}>
        <TouchableOpacity
          style={styles?.viewButtonBack}
          onPress={() => {
            navigation.openDrawer();
          }}>
          <Image style={styles?.iconMenu} source={asset.icons.menu} />
        </TouchableOpacity>
        <Text style={styles?.txtTitle}>Lịch sử chấm công</Text>
        <Image style={styles?.iconMenu} />
      </View>
      <CalendarComponent
        markedDates={listMarkedDate}
        // onGetDetailDate={(day)=>onGetDetailDate(day)}
        onGetDetailDate={onGetDetailDate}
      />
      <View style={styles?.viewRow}>
        {defaultNote.map(
          item => item?.id <= 2 && <NoteComponent {...item} />,
        )}
      </View>
      <View style={styles?.viewRow}>
        {defaultNote.map(item => item?.id > 2 && <NoteComponent {...item} />)}
      </View>
      <FlatList />

      <ActionSheet ref={actionSheetRef}>
        <View style={{height: 200, alignItems: 'center'}}>
          <Text style ={{fontSize: 16, fontWeight: 'bold'}}>Lịch sử vào / ra</Text>
          <Text style ={{fontSize: 14}}>Ngày: {timeChose?.dateString}</Text>
          {data.length === 0 ? (
            <View style={{justifyContent:'center', alignItems:'center', height : 150}}>
              <Text>No data</Text>
            </View>
          ) : (
            <View width="100%">
              <FlatList data={data} renderItem={renderItem} />
            </View>
          )}
        </View>
      </ActionSheet>
    </View>
  );
}

export default TimekeepingManager;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconMenu: {
    width: 20,
    height: 20,
  },
  txtTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
  viewHeader: {
    width: '100%',
    height: 60,
    backgroundColor: '#02488e',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginBottom: 30,
  },
  viewButtonBack: {
    width: 20,
    height: 20,
  },
  viewRow:{
    width:'100%',
    paddingHorizontal:20,
    flexDirection:'row',
    alignItems:'center',
  },
});
