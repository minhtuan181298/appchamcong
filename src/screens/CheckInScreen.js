import React, {useEffect, useState} from 'react';
import {ProgressSteps, ProgressStep} from 'react-native-progress-steps';
import {View, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import AnimatedLoader from 'react-native-animated-loader';
import asset from '../asset';
import {Check, Attendance} from '../api';
import {convertTimestampToString} from '../ulis/convertDateTime.js';

function ChekinScreen(props) {
  const [error, setError] = useState();
  const [spinning, setSpinning] = useState(false);
  const [dataUser, setDataUser] = useState({});
  const [visible, setVisible] = useState(false);
  const [success, setSuccess] = useState(true);

  const CheckFace = async () => {
    setSpinning(true);
    let data = await Check(props.route.params.data);
    if (data.error.code === 200) {
      Toast.show('Nhận dạng thành công', Toast.SHORT, Toast.TOP);
      setError(false);
      setSpinning(false);
      setDataUser(data.data);
    } else {
      Toast.show('Nhận dạng thất bại', Toast.SHORT, Toast.TOP);
      setSpinning(false);
      // setError(false);

      setError(true);
    }
  };

  const CheckSuccess = async () => {
    try {
      let data = await Attendance(dataUser?._id);
      if (data && data?.error?.code === 200) {
        setVisible(true);
        setTimeout(() => {
          Toast.show('Chấm công thành công', Toast.SHORT, Toast.TOP);
          props.navigation.navigate('beginscreen');
        }, 2500);
      } else {
        failure();
      }
    } catch (error) {
      failure();
    }
  };

  const failure = async () => {
    await setSuccess(false);
    setVisible(true);
    setTimeout(() => {
      Toast.show(
        'Chấm công thất bại . Vui lòng thử lại sau ',
        Toast.SHORT,
        Toast.TOP,
      );
      props.navigation.navigate('beginscreen');
    }, 2000);
  };

  return (
    <View style={{flex: 1, backgroundColor: '#02488e'}}>
      <Spinner visible={spinning} textStyle={styles.spinnerTextStyle} />
      <ProgressSteps
        activeStepIconBorderColor="#0080FF"
        completedStepIconColor="#0080FF"
        activeStepNumColor="white"
        completedProgressBarColor="#0080FF">
        <ProgressStep onNext={CheckFace} errors={error} nextBtnText="Tiếp tục">
          <View style={styles.viewProcessOne}>
            <Image
              style={styles.imgProcessOne}
              source={{
                uri: `data:image/jpeg;base64,${props.route.params.data}`,
              }}
            />
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.capture}>
              <Text
                style={{fontSize: 14, color: '#0080FF', fontWeight: 'bold'}}>
                {' '}
                Chụp lại
              </Text>
            </TouchableOpacity>
          </View>
        </ProgressStep>

        <ProgressStep
          previousBtnText="Trở lại"
          onNext={CheckSuccess}
          nextBtnText="Chấm công">
          <View style={styles.viewProcessTwo}>
            <Text style={styles.txt_ProcessThree}>Thông tin nhân viên</Text>
            <Image
              style={styles.imgProcessTwo}
              source={{
                uri: `data:image/jpeg;base64,${props.route.params.data}`,
              }}
            />
            <View style={{marginTop: 20, width: '60%'}}>
              <View style={styles.viewElememtTwo}>
                <Text style={styles.txt_ProcessTwo}>Họ tên : </Text>
                <Text style={styles.txt_ProcessTwo}>{dataUser.full_name}</Text>
              </View>
              {/* <View style={{ display: 'flex', flexDirection: 'row' }}>
                                <Text style={{ color: '#fff', fontSize: 15, marginRight: 10 }}>Giới tính :</Text>
                                <Text style={{ color: '#fff', fontSize: 15, marginRight: 10 }}>{dataUser.gender === 1 ? 'Nam' : dataUser.gender === 2 ? 'Nữ' : null}</Text>
                            </View> */}
              <View style={styles.viewElememtTwo}>
                <Text style={styles.txt_ProcessTwo}>Ngày sinh : </Text>
                <Text style={styles.txt_ProcessTwo}>
                  {dataUser.dob ? convertTimestampToString(dataUser.dob) : null}
                </Text>
              </View>
              <View style={styles.viewElememtTwo}>
                <Text style={styles.txt_ProcessTwo}>Số CMT : </Text>
                <Text style={styles.txt_ProcessTwo}>{dataUser.idCard}</Text>
              </View>
              <View style={styles.viewElememtTwo}>
                <Text style={styles.txt_ProcessTwo}>Chức vụ : </Text>
                <Text style={styles.txt_ProcessTwo}>
                  {dataUser.position_id
                    ? dataUser.position_id.position_name
                    : null}
                </Text>
              </View>
            </View>
          </View>
        </ProgressStep>

        <ProgressStep previousBtnText={null} finishBtnText={null}>
          <View style={{alignItems: 'center'}}>
            <AnimatedLoader
              visible={visible}
              source={
                success
                  ? require('../asset/json/9917-success.json')
                  : require('../asset/json/error.json')
              }
              animationStyle={styles.lottie}
              speed={2}
              loop={false}>
              <Text style={styles.txt_ProcessThree}>
                {success ? 'Thành công' : 'Thất bại'}
              </Text>
            </AnimatedLoader>
          </View>
        </ProgressStep>
      </ProgressSteps>
    </View>
  );
}

const styles = StyleSheet.create({
  viewProcessOne: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  viewProcessTwo: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 1,
    marginTop: 40,
    flexDirection: 'column',
  },
  viewElememtTwo: {
    flexDirection: 'row',
    marginTop: 7,
  },
  txt_ProcessTwo: {
    color: '#fff',
    fontSize: 15,
  },
  txt_ProcessThree: {
    color: '#fff',
    fontSize: 22,
    fontWeight: 'bold',
  },
  imgProcessOne: {
    width: '65%',
    height: 400,
    marginTop: 80,
    borderRadius: 10,
  },
  imgProcessTwo: {
    width: '60%',
    height: 250,
    marginTop: 50,
    borderRadius: 10,
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    // paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 30,
  },
  lottie: {
    width: 200,
    height: 200,
  },
});
export default ChekinScreen;
