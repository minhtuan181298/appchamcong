/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import asset from '../asset';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {convertTimestampToString} from '../ulis/convertDateTime.js';
import {SERVER_BASE_URL} from '../constant';
import {getItem} from '../ulis/asyncStorage';
import {getInfoEmployee} from '../api/timekeepingApis';
import {getUserInfo} from '../api';
function userInfo(props) {
  const [userInfo, setUserInfo] = useState();
  const [spinner, setSpinner] = useState(true);
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      const userInfo = await getItem('USER_INFO');
      const params = {
        userObjId: userInfo?.userObjId,
      };
      const employeeInfo = await getInfoEmployee(params);
      console.log('employeeInfo', employeeInfo);
      // const value = await AsyncStorage.getItem('data');
      // const data = await getUserInfo(value);
      setUserInfo(employeeInfo?.data);
      setSpinner(false);
    } catch (error) {
      setSpinner(false);
    }
  };
  return (
    <ScrollView>
      <Spinner visible={spinner} textStyle={styles.spinnerTextStyle} />
      <View style={styles.view_body}>
        <View style={styles.view_top}>
          <View style={styles.viewLogin}>
            <TouchableOpacity
              onPress={() => {
                props.navigation.openDrawer();
              }}>
              <Image
                style={{width: 20, height: 20, marginTop: 5}}
                source={asset.icons.menu}
              />
            </TouchableOpacity>
            <Text style={styles.txt_trueid}>Thông tin nhân viên</Text>
            <Text />
          </View>
          <Image
            style={styles.img_logo}
            source={{
              uri: `${SERVER_BASE_URL}/uploads/${userInfo?.imageSelfie}`,
            }}
          />
          <View style={styles.view_txt_login} />
        </View>
        <View style={styles.view_container}>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Domain</Text>
            <TextInput
              editable={false}
              style={styles.inp_uname}
              value={userInfo?.domain}
            />
          </View>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Họ tên</Text>
            <TextInput
              editable={false}
              value={userInfo?.name}
              style={styles.inp_uname}
            />
          </View>
          {/* <View style={styles.view_email}>
            <Text style={styles.txt_email}>Chức vụ</Text>
            <TextInput
              editable={false}
              value={userInfo?.position_id?.position_name}
              style={styles.inp_uname}
            />
          </View> */}
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Giới tính</Text>
            <TextInput
              editable={false}
              value={
                spinner ? null : userInfo?.gender === 'male' ? 'Nam' : 'Nữ'
              }
              style={styles.inp_uname}
            />
          </View>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Ngày sinh</Text>
            <TextInput
              editable={false}
              value={userInfo?.dob}
              style={styles.inp_uname}
            />
          </View>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Số CMT</Text>
            <TextInput
              editable={false}
              value={userInfo?.idCard}
              style={styles.inp_uname}
            />
          </View>
          <View style={styles.view_email}>
            <Text style={styles.txt_email}>Địa chỉ</Text>
            <TextInput
              editable={false}
              value={userInfo?.address}
              style={styles.inp_uname}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
export default userInfo;

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  view_body: {
    width: DEVICE_WIDTH,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  view_top: {
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT / 5,
    backgroundColor: '#02488e',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  view_container: {
    width: '93%',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    marginTop: 30,
    borderRadius: 5,
    paddingBottom: 15,
    padding: 5,
  },
  view_btn: {
    width: DEVICE_WIDTH,
    flexDirection: 'column',
    alignItems: 'center',
    padding: 40,
    paddingBottom: 20,
  },
  view_email: {
    width: '93%',
    flexDirection: 'column',
  },
  view_txt_login: {
    width: DEVICE_WIDTH,
    alignItems: 'center',
  },
  inp_uname: {
    width: '100%',
    height: 50,
    borderBottomColor: '#02488e',
    borderBottomWidth: 1,
    fontSize: 14,
    color: 'black',
  },
  txt_login: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  btn_login: {
    width: '93%',
    height: 50,
    backgroundColor: '#02488e',
    borderRadius: 5,
    justifyContent: 'center',
  },
  txt_btn_login: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
  btn_register: {
    width: 120,
    height: 45,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#020202',
    padding: 20,
  },
  txt_register: {
    textAlign: 'center',
    padding: 7,
    fontWeight: 'bold',
    fontSize: 18,
  },
  view_forgot_pass: {
    flexDirection: 'row',
  },
  viewLogin: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    width: DEVICE_WIDTH,
    marginTop: 20,
    paddingLeft: 10,
    alignItems: 'center',
  },

  txt_trueid: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: 'bold',
    marginLeft: -20,
  },
  txt_voice: {
    fontSize: 20,
    color: '#ffffff',
    paddingBottom: 30,
  },
  img_logo: {
    width: 110,
    height: 110,
  },
  txt_email: {
    fontSize: 14,
    color: '#C7C7CF',
    paddingTop: 5,
  },
  txt_signup: {
    fontSize: 16,
    color: '#02488e',
    fontWeight: 'bold',
  },
  txt_donthave: {
    fontSize: 16,
    color: '#02488e',
  },
  txt_forgot_pass: {
    fontSize: 16,
    color: '#020202',
  },
  btn_signup: {
    flexDirection: 'column',
    marginTop: 0,
    alignItems: 'center',
  },
});
