import {
  axiosBodyToAPI,
  sendQueryToAPI,
  sendQueryChatToAPI,
} from '../ulis/servicesApi';

import {FETCH_LIST_TIMEKEEPING} from '../api/endPoint';
import {SERVER_BASE_URL} from '../constant';

const fetchListDateByUser = async params => {
  try {
    const result = await sendQueryChatToAPI(
      'GET',
      FETCH_LIST_TIMEKEEPING,
      null,
      params,
    );
    if (result?.data?.success) {
      return {data: result?.data?.data, error: false};
    } else {
      return {data: false, error: true};
    }
  } catch (error) {
    return {data: false, error: true};
  }
};

const GetDetailCheckout = async (objId, date) => {
  try {
    const result = await sendQueryChatToAPI(
      'GET',
      `${SERVER_BASE_URL}/mobile/timekeeping/detail?userObjId=${objId}&date=${date}`,
      null,
    );
    if (result?.data?.success) {
      return {data: result?.data?.data, error: false};
    } else {
      return {data: false, error: true};
    }
  } catch (error) {
    return {data: false, error: true};
  }
};

const changePassword = async payload => {
  try {
    const result = await sendQueryChatToAPI(
      'PUT',
      `${SERVER_BASE_URL}/admin/users/changePassword`,
      payload,
    );
    if (result?.data?.success) {
      return {data: result?.data?.data, error: false};
    } else {
      return {data: false, error: true};
    }
  } catch (error) {
    return {data: false, error: true};
  }
};

const getInfoEmployee = async params => {
  try {
    const result = await sendQueryChatToAPI(
      'GET',
      `${SERVER_BASE_URL}/mobile/users/info`,
      null,
      params,
    );
    if (result?.data?.success) {
      return {data: result?.data?.data, error: false};
    } else {
      return {data: false, error: true};
    }
  } catch (error) {
    return {data: false, error: true};
  }
};
// //fetch detail timekeeping by user in date
// const fetchDetailTimekeepingDate = async params => {
//   try {
//     const result = await sendQueryChatToAPI(
//       'GET',
//       DETAIL_TIMEKEEPING_DATE,
//       null,
//       params,
//     );
//     console.log({result});
//     if (result?.data?.success) {
//       return {data: result?.data?.data, error: false};
//     } else {
//       return {data: false, error: true};
//     }
//   } catch (error) {
//     return {data: false, error: true};
//   }
// };
// //report list by Month
// const fetchReportByMonth = async params => {
//   try {
//     const result = await sendQueryChatToAPI(
//       'GET',
//       REPORT_LIST_BY_MONTH,
//       null,
//       params,
//     );
//     console.log({result});
//     if (result?.data?.success) {
//       return {data: result?.data?.data, error: false};
//     } else {
//       return {data: false, error: true};
//     }
//   } catch (error) {
//     return {data: false, error: true};
//   }
// };

export {
  fetchListDateByUser,
  GetDetailCheckout,
  changePassword,
  getInfoEmployee,
  // fetchDetailTimekeepingDate, fetchReportByMonth
};
