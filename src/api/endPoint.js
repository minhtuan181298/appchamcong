import {BASE_URL, SERVER_BASE_URL} from '../constant';

export const LOGIN_URL = `${SERVER_BASE_URL}/signIn`;

//timekeeping
export const FETCH_LIST_TIMEKEEPING = `${SERVER_BASE_URL}/mobile/timekeeping/listByUser`;
