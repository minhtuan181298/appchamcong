import axios from 'axios';
import {setToken, setUserInfo} from '../ulis/asyncStorage';

const {SERVER_BASE_URL} = require('../constant');

const signIn = async params => {
  try {
    const {username, password} = params;
    const headers = {
      'Content-Type': 'application/json',
    };
    const result = await axios.post(
      `${SERVER_BASE_URL}/signIn`,
      {username, password},
      {headers},
    );
    if (result && result.data.success) {
      const data = result.data.data;
      await setToken(data.token);
      await setUserInfo(data);
    }
    return result.data.data;
  } catch (error) {
    return false;
  }
};

export {signIn};
