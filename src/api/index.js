import axios from 'axios';
import {BASE_URL, SERVER_BASE_URL} from '../constant';

export const Check = async image => {
  const {data} = await axios.post(`${BASE_URL}/check`, {image: image});
  return data;
};

export const GetPosition = async () => {
  const {data} = await axios.get(`${SERVER_BASE_URL}/admin/position`);
  return data;
};

export const Attendance = async employee_id => {
  const {data} = await axios.post(`${SERVER_BASE_URL}/timekeeping`, {
    employee_id,
    location: '',
  });
  return data;
};

export const Login = async (username, password) => {
  const {data} = await axios.post(`${SERVER_BASE_URL}/login`, {
    username,
    password,
  });
  return data;
};
export const onChangePassword = async (id, oldPassword, newPassword) => {
  const {data} = await axios.put(
    `${SERVER_BASE_URL}/employees/password/change/${id}`,
    {
      oldPassword,
      newPassword,
    },
  );
  return data;
};

export const getUserInfo = async id => {
  const {data} = await axios.get(`${SERVER_BASE_URL}/employees/${id}`);
  return data;
};

export const getTimekeeping = async id => {
  const {data} = await axios.get(`${SERVER_BASE_URL}/timekeeping/${id}`);
  return data;
};
