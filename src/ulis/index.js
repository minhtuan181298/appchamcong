import {colors} from '../constant';

const renderColor = status => {
  switch (status) {
    case 'SUCCEED':
      return colors?.green;

    case 'FAILED':
      return colors?.red;

    case 'LATE':
      return colors?.yellow;

    case 'HALF':
      return colors?.violet;

    default:
      return colors?.white;
  }
};

export {renderColor};
