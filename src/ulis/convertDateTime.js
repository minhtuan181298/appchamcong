const moment = require('moment');

//timestamp to datetime
export const convertTimestampToDatetime = timestamp => {
  var newDate = new Date(timestamp);
  return `${newDate.getDate() +
    '/' +
    (newDate.getMonth() + 1) +
    '/' +
    newDate.getFullYear()}`;
};
export const convertTimestampToStringTime = timestamp => {
  var dateTimeString = moment(timestamp).format('HH:mm');
  return dateTimeString;
};

export const convertTimestampToString = timestamp => {
  var dateTimeString = moment(timestamp * 1000).format('DD-MM-YYYY');
  return dateTimeString;
};
export const convertDateToDay = date => {
  let day;
  switch (date) {
    case 'Sun':
      day = 'Chủ nhật';
      break;
    case 'Mon':
      day = 'Thứ hai';
      break;
    case 'Tue':
      day = 'Thứ ba';
      break;
    case 'Wed':
      day = 'Thứ tư';
      break;
    case 'Thu':
      day = 'Thứ năm';
      break;
    case 'Fri':
      day = 'Thứ sáu';
      break;
    case 'Sat':
      day = 'Thứ bảy';
      break;
    default:
    // code block
  }
  return day;
};
