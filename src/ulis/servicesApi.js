import axios from 'axios';
import {getToken} from '../ulis/asyncStorage';

const name = 'name';
const pass = 'name';

export const axiosBodyToAPI = async (method, uri, body, json = true) => {
  try {
    const token = await getToken();
    const headerConfig = {
      headers: {
        access_token: token,
        'Content-Type': 'application/json',
      },
    };
    let result;
    if (method === 'POST') {
      result = await axios.post(uri, body, headerConfig);
    } else if (method === 'PUT') {
      result = await axios.put(uri, body, headerConfig);
    } else if (method === 'DELETE') {
      const config = {
        ...headerConfig,
        data: body,
      };
      result = await axios.delete(uri, config);
    } else {
      result = await axios.post(uri, body, headerConfig);
    }
    return result;
  } catch (error) {
    return error;
  }
};

export const sendQueryToAPI = async uri => {
  try {
    const token = await getToken();
    const headerConfig = {
      headers: {
        access_token: token,
      },
    };
    const result = await axios.get(uri, headerConfig);
    return result;
  } catch (error) {
    return error;
  }
};

//delete
export const sendQueryToPutAPI = async uri => {
  try {
    const token = await getToken();
    const headerConfig = {
      headers: {
        access_token: token,
      },
    };
    const result = await axios.put(uri, headerConfig);
    return result;
  } catch (error) {
    return error;
  }
};

//delete
export const axiosFormDataBodyToAPI = async (
  method,
  uri,
  body,
  json = true,
) => {
  try {
    const token = await getToken();
    const headerConfig = {
      headers: {
        access_token: token,
        'Content-Type': 'multipart/form-data',
      },
    };
    let result;
    if (method === 'POST') {
      result = await axios.post(uri, body, headerConfig);
    } else if (method === 'PUT') {
      result = await axios.put(uri, body, headerConfig);
    } else if (method === 'DELETE') {
      const config = {
        ...headerConfig,
        data: body,
      };
      result = await axios.delete(uri, config);
    } else {
      result = await axios.post(uri, body, headerConfig);
    }
    return result;
  } catch (error) {
    return error;
  }
};

export const sendQueryChatToAPI = async (method, uri, body, query) => {
  try {
    const token = await getToken();
    const headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        access_token: `${token}`,
      },
    };
    let result;
    switch (method) {
      case 'GET':
        headerConfig.params = query;
        result = await axios.get(uri, headerConfig);
        return result;

      case 'POST':
        result = await axios.post(uri, body, headerConfig);
        return result;

      case 'PUT':
        result = await axios.put(uri, body, headerConfig);
        return result;

      case 'DELETE':
        headerConfig.params = query;
        result = await axios.delete(uri, headerConfig);
        return result;
      default:
        break;
    }
  } catch (error) {
    return error;
  }
};
