import AsyncStorage from '@react-native-async-storage/async-storage';

//gettoken
export const getToken = async () => {
  try {
    const value = await AsyncStorage.getItem('TOKEN');
    if (value !== null) {
      return JSON.parse(value);
    }
  } catch (e) {
    console.log('error', e);
  }
  return null;
};
//set Token
export const setToken = async value => {
  try {
    const json = JSON.stringify(value);
    await AsyncStorage.setItem('TOKEN', json);
  } catch (e) {
    console.log('error setItem', e);
  }
};

//get userInfo
export const getUserInfo = async () => {
  try {
    const value = await AsyncStorage.getItem('USER_INFO');
    if (value !== null) {
      return JSON.parse(value);
    }
  } catch (e) {
    console.log('error', e);
  }
  return null;
};
//set userInfo
export const setUserInfo = async value => {
  try {
    const json = JSON.stringify(value);
    await AsyncStorage.setItem('USER_INFO', json);
  } catch (e) {
    console.log('error setItem', e);
  }
};

export const getItem = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return JSON.parse(value);
    }
  } catch (e) {
    console.log('error', e);
  }
  return null;
};

export const removeItem = async key => {
  try {
    await AsyncStorage.removeItem(key);
    // console.log('remove succ')
  } catch (e) {
    console.log('error', e);
  }
};

export const removeAll = async () => await AsyncStorage.clear();

export const setItem = async (key, value) => {
  try {
    const json = JSON.stringify(value);
    await AsyncStorage.setItem(key, json);
  } catch (e) {
    console.log('error setItem', e);
  }
};
