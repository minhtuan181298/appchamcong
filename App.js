import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
const HomeStack = createStackNavigator();

import BeginScreen from './src/screens/BeginScreen';
import CheckinScreen from './src/screens/CheckInScreen';
import LoginScreen from './src/screens/LoginScreen';
import CameraScreen from './src/screens/CameraScreen';
import HistoryScreen from './src/screens/HistoryScreen';
import ChangePassword from './src/screens/ChangePassword';
import userInfo from './src/screens/UserInfo';
import BaseDrawer from './src/component/BaseDrawer';
import TimekeepingManager from './src/screens/TimekeepingManager';
import CheckoutToday from './src/screens/CheckoutToDay';
import {getUserInfo} from './src/ulis/asyncStorage';

const Drawer = createDrawerNavigator();

function user() {
  return (
    <Drawer.Navigator drawerContent={props => <BaseDrawer {...props} />}>
      <Drawer.Screen name="timekeepingManager" component={TimekeepingManager} />
      <Drawer.Screen name="historyScreen" component={HistoryScreen} />
      <Drawer.Screen name="ChangePassword" component={ChangePassword} />
      <Drawer.Screen name="userInfo" component={userInfo} />
      <Drawer.Screen name="CheckoutToday" component={CheckoutToday} />
    </Drawer.Navigator>
  );
}

function App() {
  const [userInfoData, setUserInfoData] = useState();
  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    const userData = getUserInfo();
    setUserInfoData(userData);
  };
  return (
    <NavigationContainer>
      <HomeStack.Navigator screenOptions={{headerShown: false}}>
        {userInfoData ? (
          <HomeStack.Screen name="user" component={user} />
        ) : (
          <HomeStack.Screen name="beginscreen" component={BeginScreen} />
        )}
        {/* <HomeStack.Screen name="beginscreen" component={BeginScreen} /> */}
        <HomeStack.Screen name="camerascreen" component={CameraScreen} />
        <HomeStack.Screen name="loginscreen" component={LoginScreen} />
        {/* <HomeStack.Screen name="user" component={user} /> */}

        {/* <HomeStack.Screen name="historyScreen" component={HistoryScreen} />
        <HomeStack.Screen name="ChangePassword" component={ChangePassword} /> */}
        <HomeStack.Screen name="checkinscreen" component={CheckinScreen} />
      </HomeStack.Navigator>
    </NavigationContainer>
  );
}

export default App;
